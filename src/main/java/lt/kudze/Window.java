package lt.kudze;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowListener;

public class Window extends JFrame {

    public Window(String title, Component rootComponent) {
        this(title, rootComponent, null);
    }

    public Window(String title, Component rootComponent, WindowListener windowListener) {
        this(title, rootComponent, windowListener, false);
    }

    public Window(String title, Component rootComponent, WindowListener windowListener, boolean resizeable) {
        super();

        if (rootComponent != null)
            this.add(rootComponent);
        this.setTitle(title);

        if (windowListener == null)
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        else {
            this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            this.addWindowListener(windowListener);
        }

        this.pack();
        this.setResizable(resizeable);
        this.setVisible(true);
    }

    public void close() {
        this.setVisible(false);
        this.dispose();
    }
}
