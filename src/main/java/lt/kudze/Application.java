package lt.kudze;

import lt.kudze.data.Configuration;
import lt.kudze.panels.CanvasPanel;
import lt.kudze.panels.ConfigurationPanel;
import lt.kudze.panels.ConfigurationPanelConfiguredInterface;

public class Application implements ConfigurationPanelConfiguredInterface {
    public static final String WINDOW_TITLE = "Karolis Kraujelis | Fraktalai 4";
    public static final String WINDOW_CONFIG_TITLE = "Karolis Kraujelis | Fraktalai 4 | Konfiguracija";

    protected Window window;

    public Application() {
        initializeConfigureWindow();
    }

    protected void initializeConfigureWindow() {
        this.window = new Window(WINDOW_CONFIG_TITLE, new ConfigurationPanel(this, this), null, true);
    }

    protected void initializeAppWindow(Configuration configuration) {
        this.window = new Window(WINDOW_TITLE, new CanvasPanel(configuration));
    }

    @Override
    public void onConfigured(Configuration config) {
        this.window.close();
        this.initializeAppWindow(config);
    }

    public Window getWindow() {
        return window;
    }
}
