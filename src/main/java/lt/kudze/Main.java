package lt.kudze;

import lt.kudze.panels.ConfigurationPanel;

/**
 *  Praktinė užduotis. Sudarykite programą, kuria būtų modeliuojamas L-medis. Programos interfeise
 *  įvedama aksioma ir reprodukcijos taisyklė.
 *  Paspaudus ”enter” mygtuką, rekursyviai turi ”augti” L-sistemos realizacija.
 *  Pademonstruokite L-sistemą, kuria būtu modeliuojama erdvę užpildanti kreivė.
 *
 *  Kodavimas:
 *  F - brėžti, judėti tiesiai
 *  L - nebrėžti, judėti tiesiai
 *  + pasukti kampu delta
 *  - pasukti delta
 *  [] - viduj gali but komandos po to gryžta.
 */
public class Main {

    public static void main(String[] args) {
        new Application();
    }
}