package lt.kudze.data;

import java.util.Map;

public class Configuration {
    public static final String RULE_WHITELIST = "FL+-[]";

    public static String validateRule(String rule) {
        return validateRule(rule, "");
    }

    /**
     * If rule is valid returns null.
     *
     * @return ?String
     */
    public static String validateRule(String rule, String customRules) {
        if (rule.isEmpty())
            return "Taiskylė negali būti tuščia!";

        String whitelist = RULE_WHITELIST + customRules;

        int branching = 0;
        for (char letter : rule.toCharArray()) {
            //Validating rules.
            if (!whitelist.contains(String.valueOf(letter)))
                return "Taisyklėje rasta neatpažinta taisyklė: \"" + letter + "\"!";

            //Two seperate cases for open and close branching.
            if (letter == '[') {
                branching++;
                continue;
            }

            if (letter == ']') {
                branching--;

                if (branching < 0)
                    return "Taisyklėje \"]\" yra prieš \"[\"!";
            }
        }

        if (branching != 0)
            return "Taiskylėje \"[\" kiekis su \"]\" nesutampa!";

        return null;
    }

    protected String axiom;
    protected Map<Character, String> rules;
    protected Map<Character, String> graphics;

    protected double length;
    protected double lengthFactor;
    protected double delta;

    public Configuration(String axiom, Map<Character, String> rules, Map<Character, String> graphics, double length, double lengthFactor, double delta) {
        this.axiom = axiom;
        this.rules = rules;
        this.graphics = graphics;
        this.lengthFactor = lengthFactor;
        this.length = length;
        this.delta = delta;
    }

    public void iterate() {
        StringBuilder builder = new StringBuilder();

        for (char rule : this.axiom.toCharArray()) {
            if (this.rules.containsKey(rule))
                builder.append(this.rules.get(rule));
            else
                builder.append(rule);
        }

        this.axiom = builder.toString();
        this.length *= this.lengthFactor;
    }

    public String getAxiom() {
        return axiom;
    }

    public String getRenderAxiom() {
        //render axiom is axiom with replaced graphics.
        StringBuilder builder = new StringBuilder();

        for(char rule : this.axiom.toCharArray()) {
            if(this.graphics.containsKey(rule))
                builder.append(graphics.get(rule));
            else
                builder.append(rule);
        }

        return builder.toString();
    }

    public void setAxiom(String axiom) {
        this.axiom = axiom;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getDelta() {
        return delta;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }
}
