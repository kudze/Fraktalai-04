package lt.kudze.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ConfigurationRenderer {

    public static List<Line> renderConfiguration(Configuration configuration) {
        double deltaRad = Math.toRadians(configuration.getDelta());
        String renderAxiom = configuration.getRenderAxiom();

        TurtleState turtleState = new TurtleState();
        Stack<TurtleState> branchPositions = new Stack<>();
        ArrayList<Line> result = new ArrayList<>(calculateLinesInAxiom(renderAxiom));

        for (char rule : renderAxiom.toCharArray()) {
            if (rule == '+') {
                turtleState.addAngle(deltaRad);
                continue;
            }

            if (rule == '-') {
                turtleState.subAngle(deltaRad);
                continue;
            }

            if (rule == '[') {
                branchPositions.push(new TurtleState(turtleState));
                continue;
            }

            if (rule == ']') {
                turtleState = branchPositions.pop();
                continue;
            }

            //Otherwise if F or L.
            Point turtlePosition = turtleState.getPosition();
            double turtleAngle = turtleState.getAngleRad();

            Point startPosition = new Point(turtlePosition);
            turtlePosition.setX(turtlePosition.getX() + Math.cos(turtleAngle) * configuration.getLength());
            turtlePosition.setY(turtlePosition.getY() + Math.sin(turtleAngle) * configuration.getLength());

            if (rule == 'F')
                result.add(new Line(startPosition, new Point(turtlePosition)));
        }

        return result;
    }

    public static int calculateLinesInAxiom(String axiom) {
        int result = 0;

        for (char rule : axiom.toCharArray())
            if (rule == 'F')
                result++;

        return result;
    }

}
