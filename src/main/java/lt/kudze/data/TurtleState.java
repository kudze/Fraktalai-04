package lt.kudze.data;

public class TurtleState {
    protected Point position;
    protected double angleRad;

    public TurtleState() {
        this(new Point(0.5, 0), Math.toRadians(90));
    }

    public TurtleState(Point position, double angleRad) {
        this.position = position;
        this.angleRad = angleRad;
    }

    public TurtleState(TurtleState turtleState) {
        this(new Point(turtleState.getPosition()), turtleState.getAngleRad());
    }

    public Point getPosition() {
        return position;
    }

    public double getAngleRad() {
        return angleRad;
    }

    public void setAngleRad(double angleRad) {
        this.angleRad = angleRad;
    }

    public void addAngle(double angleRad) {
        this.angleRad += angleRad;
    }

    public void subAngle(double angleRad) {
        this.angleRad -= angleRad;
    }
}
