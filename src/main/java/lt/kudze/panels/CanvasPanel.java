package lt.kudze.panels;

import lt.kudze.data.Configuration;
import lt.kudze.data.ConfigurationRenderer;
import lt.kudze.data.Line;
import lt.kudze.data.Point;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class CanvasPanel extends OrangePanel implements KeyListener {
    public static final int WIDTH = 600;
    public static final int HEIGHT = 600;
    public static final Color SHAPE_COLOR = new Color(0, 0, 0);

    protected Configuration configuration;

    public CanvasPanel(Configuration configuration) {
        super();

        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.setFocusable(true);
        this.addKeyListener(this);
        this.grabFocus();

        this.configuration = configuration;
    }

    protected void iterate() {
        this.configuration.iterate();
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        int width = this.getWidth();
        int height = this.getHeight();

        g.setColor(this.getBackground());
        g.fillRect(
                0, 0,
                width, height
        );

        g.setColor(SHAPE_COLOR);
        for (Line line : ConfigurationRenderer.renderConfiguration(this.configuration)) {
            line = transformToScreen(line);

            int sx = (int) Math.round(line.getStart().getX()),
                    sy = (int) Math.round(line.getStart().getY()),
                    ex = (int) Math.round(line.getEnd().getX()),
                    ey = (int) Math.round(line.getEnd().getY());

            //System.out.println("Line: Start {" + sx + ", " + sy + "}, End {" + ex + ", " + ey + "}");

            g.drawLine(sx, sy, ex, ey);
        }
    }

    protected Line transformToScreen(Line line) {
        int scaleFactor = this.getWidth();

        RealMatrix scaleMatrix = MatrixUtils.createRealMatrix(new double[][]{
                {scaleFactor, 0},
                {0, -scaleFactor},
        });

        return new Line(
                transform(scaleMatrix, line.getStart(), new Point(0, scaleFactor)),
                transform(scaleMatrix, line.getEnd(), new Point(0, scaleFactor))
        );
    }

    protected Point transform(RealMatrix matrix, Point point) {
        return new Point(matrix.multiply(point.getVector()));
    }

    protected Point transform(RealMatrix matrix, Point point, Point postOffset) {
        point = transform(matrix, point);
        point.setX(point.getX() + postOffset.getX());
        point.setY(point.getY() + postOffset.getY());
        return point;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() != 10)
            return;

        this.iterate();
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
