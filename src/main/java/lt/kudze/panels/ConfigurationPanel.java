package lt.kudze.panels;

import lt.kudze.Application;
import lt.kudze.components.RedLabel;
import lt.kudze.data.Configuration;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationPanel extends GrayPanel {
    protected ConfigurationPanelConfiguredInterface subscriber;

    protected ConfigurationGroupPanel configurationGroupPanel;

    protected JTextField axiomInput = new JTextField();
    protected JTextField ruleFInput = new JTextField("F");
    protected JTextField ruleLInput = new JTextField("L");
    protected JTextField rulePlusInput = new JTextField("+");
    protected JTextField ruleMinusInput = new JTextField("-");
    protected JTextField deltaInput = new JTextField("90");
    protected JTextField lengthInput = new JTextField("0.5");
    protected JTextField nInput = new JTextField("1");

    protected JLabel axiomValidationLabel = new RedLabel("");
    protected JLabel ruleFValidationLabel = new RedLabel("");
    protected JLabel ruleLValidationLabel = new RedLabel("");
    protected JLabel rulePlusValidationLabel = new RedLabel("");
    protected JLabel ruleMinusValidationLabel = new RedLabel("");
    protected JLabel deltaValidationLabel = new RedLabel("");
    protected JLabel lValidationLabel = new RedLabel("");
    protected JLabel nValidationLabel = new RedLabel("");

    public ConfigurationPanel(ConfigurationPanelConfiguredInterface subscriber, Application app) {
        super();

        this.subscriber = subscriber;
        this.configurationGroupPanel = new ConfigurationGroupPanel(app);

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(new EmptyBorder(20, 20, 20, 20));

        this.add(new InputPanel("Aksioma:", this.axiomInput, this.axiomValidationLabel));
        this.add(new InputPanel("Reprodukcijos taiskylė F:", this.ruleFInput, this.ruleFValidationLabel));
        this.add(new InputPanel("Reprodukcijos taiskylė L:", this.ruleLInput, this.ruleLValidationLabel));
        this.add(new InputPanel("Reprodukcijos taiskylė +:", this.rulePlusInput, this.rulePlusValidationLabel));
        this.add(new InputPanel("Reprodukcijos taiskylė -:", this.ruleMinusInput, this.ruleMinusValidationLabel));
        this.add(new InputPanel("Delta:", this.deltaInput, this.deltaValidationLabel));
        this.add(new InputPanel("l:", this.lengthInput, this.lValidationLabel));
        this.add(new InputPanel("n:", this.nInput, this.nValidationLabel));
        this.add(this.configurationGroupPanel);


        this.add(this.makeConfigureButton());
    }

    public void save() {
        //Validation.

        //Clear validation errors.
        this.axiomValidationLabel.setText("");
        this.ruleFValidationLabel.setText("");
        this.ruleLValidationLabel.setText("");
        this.rulePlusValidationLabel.setText("");
        this.ruleMinusValidationLabel.setText("");
        this.deltaValidationLabel.setText("");
        this.lValidationLabel.setText("");
        this.nValidationLabel.setText("");

        //Read values.
        String axiom = this.axiomInput.getText();
        String ruleF = this.ruleFInput.getText();
        String ruleL = this.ruleLInput.getText();
        String rulePlus = this.rulePlusInput.getText();
        String ruleMinus = this.ruleMinusInput.getText();
        String deltaInput = this.deltaInput.getText();
        String lInput = this.lengthInput.getText();
        String nInput = this.nInput.getText();

        //Validation iself.
        boolean pass = true;

        //At first we have to validate the custom rules itself, to collect custom rules list.
        String customRules = "";
        for (ConfigurationGroupRowPanel row : this.configurationGroupPanel.getRows()) {
            row.getIdValidationLabel().setText("");
            String ruleIndex = row.getIdField().getText();

            if (ruleIndex.length() != 1) {
                row.getIdValidationLabel().setText("Taisyklė privalo būti tik 1 simbolis!");
                pass = false;
                continue;
            }

            if ((customRules + Configuration.RULE_WHITELIST).contains(String.valueOf(ruleIndex))) {
                row.getIdValidationLabel().setText("Taisyklė privalo būti unikali!");
                pass = false;
                continue;
            }

            customRules += ruleIndex;
        }

        String axiomValidationResult = Configuration.validateRule(axiom, customRules);
        String ruleFValidationResult = Configuration.validateRule(ruleF, customRules);
        String ruleLValidationResult = Configuration.validateRule(ruleL, customRules);
        String rulePlusValidationResult = Configuration.validateRule(rulePlus, customRules);
        String ruleMinusValidationResult = Configuration.validateRule(ruleMinus, customRules);
        String deltaValidationResult = null;
        String lValidationResult = null;
        String nValidationResult = null;

        double delta = 0, l = 0, n = 0;

        try {
            delta = Double.parseDouble(deltaInput);
        } catch (NumberFormatException e) {
            deltaValidationResult = "Įvestis turi būti skaičius!";
        }

        try {
            l = Double.parseDouble(lInput);
        } catch (NumberFormatException e) {
            lValidationResult = "Įvestis turi būti skaičius!";
        }

        try {
            n = Double.parseDouble(nInput);

            if (n <= 0) {
                nValidationResult = "n turi buti teigiamas skaičius!";
            }
        } catch (NumberFormatException e) {
            nValidationResult = "Įvestis turi būti skaičius!";
        }

        HashMap<Character, String> rules = new HashMap<>();
        HashMap<Character, String> graphics = new HashMap<>();
        for (ConfigurationGroupRowPanel row : this.configurationGroupPanel.getRows()) {
            row.getRuleValidationLabel().setText("");
            row.getGraphicsValidationLabel().setText("");

            char ruleIndex = row.getIdField().getText().charAt(0);
            String rule = row.getRuleField().getText();
            String graphic = row.getGraphicsField().getText();

            String ruleValidationResult = Configuration.validateRule(rule, customRules);
            String graphicValidationResult = Configuration.validateRule(graphic);

            if (ruleValidationResult != null) {
                row.getRuleValidationLabel().setText(ruleValidationResult);
                pass = false;
            }

            if (graphicValidationResult != null) {
                row.getGraphicsValidationLabel().setText(graphicValidationResult);
                pass = false;
            }

            rules.put(ruleIndex, rule);
            graphics.put(ruleIndex, graphic);
        }

        //Assign validation errors to ui.
        if (axiomValidationResult != null) {
            this.axiomValidationLabel.setText(axiomValidationResult);
            pass = false;
        }

        if (ruleFValidationResult != null) {
            this.ruleFValidationLabel.setText(ruleFValidationResult);
            pass = false;
        }

        if (ruleLValidationResult != null) {
            this.ruleLValidationLabel.setText(ruleLValidationResult);
            pass = false;
        }

        if (rulePlusValidationResult != null) {
            this.rulePlusValidationLabel.setText(rulePlusValidationResult);
            pass = false;
        }

        if (ruleMinusValidationResult != null) {
            this.ruleMinusValidationLabel.setText(ruleMinusValidationResult);
            pass = false;
        }

        if (deltaValidationResult != null) {
            this.deltaValidationLabel.setText(deltaValidationResult);
            pass = false;
        }

        if (lValidationResult != null) {
            this.lValidationLabel.setText(lValidationResult);
            pass = false;
        }

        if (nValidationResult != null) {
            this.nValidationLabel.setText(nValidationResult);
        }

        if (!pass)
            return;

        //Save.
        rules.put('F', ruleF);
        rules.put('L', ruleL);
        rules.put('+', rulePlus);
        rules.put('-', ruleMinus);
        this.subscriber.onConfigured(new Configuration(axiom, rules, graphics, 1 / n, l, delta));
    }

    protected JComponent makeConfigureButton() {
        JButton button = new JButton("Konfiguruoti");
        button.addActionListener(new ConfigureButtonAction(this));

        JPanel panel = new GrayPanel();
        panel.add(button);
        return panel;
    }
}

record ConfigureButtonAction(ConfigurationPanel panel) implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        panel.save();
    }
}