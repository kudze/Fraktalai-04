package lt.kudze.panels;

import javax.swing.*;
import java.awt.*;

public class GrayPanel extends JPanel {
    public static final Color BG_GRAY = new Color(60, 60, 60);

    public GrayPanel() {
        this.setBackground(BG_GRAY);
    }
}
