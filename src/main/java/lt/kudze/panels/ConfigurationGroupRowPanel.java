package lt.kudze.panels;

import lt.kudze.components.RedLabel;

import javax.swing.*;
import java.awt.*;

public class ConfigurationGroupRowPanel extends GrayPanel {
    protected JTextField idField = new JTextField();
    protected JTextField ruleField = new JTextField();
    protected JTextField graphicsField = new JTextField();

    protected JLabel idValidationLabel = new RedLabel("");
    protected JLabel ruleValidationLabel = new RedLabel("");
    protected JLabel graphicsValidationLabel = new RedLabel("");

    public ConfigurationGroupRowPanel() {
        this.setLayout(new GridLayout(1, 0, 20, 0));

        this.add(new InputPanel("Taiskylė:", idField, idValidationLabel));
        this.add(new InputPanel("Reprodukcijos taisyklė:", ruleField, ruleValidationLabel));
        this.add(new InputPanel("Grafika:", graphicsField, graphicsValidationLabel));
    }

    public JTextField getIdField() {
        return idField;
    }

    public JTextField getRuleField() {
        return ruleField;
    }

    public JTextField getGraphicsField() {
        return graphicsField;
    }

    public JLabel getIdValidationLabel() {
        return idValidationLabel;
    }

    public JLabel getRuleValidationLabel() {
        return ruleValidationLabel;
    }

    public JLabel getGraphicsValidationLabel() {
        return graphicsValidationLabel;
    }
}
