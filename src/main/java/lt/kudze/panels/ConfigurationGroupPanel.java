package lt.kudze.panels;

import lt.kudze.Application;
import lt.kudze.components.WhiteLabel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ConfigurationGroupPanel extends GrayPanel {
    protected ArrayList<ConfigurationGroupRowPanel> rows = new ArrayList<>();
    protected Application app;

    public ConfigurationGroupPanel(Application app) {
        this.app = app;

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.initContents();
    }

    public void addRow() {
        ConfigurationGroupRowPanel panel = new ConfigurationGroupRowPanel();
        this.rows.add(panel);

        this.removeAll();
        this.initContents();
        this.revalidate();
        this.repaint();

        this.app.getWindow().pack();
    }

    protected void initContents() {
        JPanel panel = new GrayPanel();
        panel.add(new WhiteLabel("Papildomos taiskylės:"));
        this.add(panel);

        for (ConfigurationGroupRowPanel row : rows)
            this.add(row);

        this.add(this.makeButton("Pridėti", new ConfigurationGroupAddAction(this)));
    }

    protected JComponent makeButton(String title, ActionListener actionListener) {
        JButton button = new JButton(title);
        button.addActionListener(actionListener);

        JPanel panel = new GrayPanel();
        panel.add(button);
        return panel;
    }

    public ArrayList<ConfigurationGroupRowPanel> getRows() {
        return rows;
    }
}

record ConfigurationGroupAddAction(ConfigurationGroupPanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.addRow();
    }
}
