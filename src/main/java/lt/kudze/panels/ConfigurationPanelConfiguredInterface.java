package lt.kudze.panels;

import lt.kudze.data.Configuration;

public interface ConfigurationPanelConfiguredInterface {

    void onConfigured(Configuration config);

}
